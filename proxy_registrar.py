import sys
import socketserver
import socket
import time
import json
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

usser_dicc = {}


class UApHandler(ContentHandler):

    def __init__(self):

        self.list = []
        self.log_proxy = False
        self.d = {"server": ["name", "ip", "puerto"],
                  "database": ["path", "passwdpath"]
                  }

    def startElement(self, name, attrs):
        if name in self.d:
            list = {}
            for i in self.d[name]:
                list[i] = attrs.get(i, "")
            self.list.append([name, list])
        elif name == "log":
            self.log_proxy = True

    def characters(self, char):
        # al encontrar etiqueta sin atributo lee entre medias
        if self.log_proxy is True:
            self.list.append(["log", {"log": char}])
            self.log_proxy = False

    def get_tags(self):
        return self.list


def write_log(evento, log):
    fichero = open(log, 'a')
    tiempo = time.strftime('%Y%m%d%H%M%S')
    fichero.write(tiempo)
    fichero.write(' ')
    fichero.write(evento)
    fichero.write("\r\n")
    fichero.close()


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class ACTIVA AL RECIBIR PAQUETE
    """
    diccionario = {}

    def putuser(self, dirsip, timeexpires, expires, clientport):
        ipClient, puertoClient = self.client_address
        self.diccionario[dirsip] = {'address': ipClient,
                                    'port': clientport,
                                    'expires': timeexpires,
                                    'time expire': expires}
        print(self.diccionario)
        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')

    def deleteuser(self, dirsip):
        try:
            del self.diccionario[dirsip]
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        except KeyError:
            self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
        print(self.diccionario)

    def register2json(self):
        with open("registred.json", 'w') as f:
            json.dump(self.diccionario, f, indent=4)

    def json2register(self):
        try:
            with open('registered.json', 'r') as file:
                self.diccionario = json.load(file)
        except FileNotFoundError:
            pass

    def caducidadexpires(self):
        listaUsuarios = list(self.diccionario)
        for usuario in listaUsuarios:
            timeexpires = self.diccionario[usuario]["expires"]
            print(timeexpires)
            tiempoReal = time.strftime('%Y-%m-%d %H:%M:%S +0000',
                                       time.localtime())
            print(tiempoReal)
            if timeexpires < tiempoReal:
                del self.diccionario[usuario]
        self.register2json()

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            print("El cliente nos manda " + line.decode('utf-8'))
            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
            messageSplit = line.decode('utf-8')
            method = messageSplit.split(' ')[0]
            if method == "REGISTER":
                messageSplits = messageSplit.split(' ')
                expires = messageSplits[3]
                mensaje = messageSplits[2].split('\r\n')[1]  # expires:
                messageSplitc = messageSplits[1].split(':')
                dirSip = messageSplitc[1]
                clientPort = messageSplitc[2]
                if mensaje == 'Expires:':
                    timeExpires = int(expires)
                    if timeExpires == 0:
                        self.deleteuser(dirSip)
                    elif timeExpires > 0:
                        timeExpires = timeExpires + time.time()
                        timeExpires = time.strftime(
                            '%Y-%m-%d %H:%M:%S +0000',
                            time.localtime(timeExpires))
                        self.putuser(dirSip, timeExpires,
                                     expires, clientPort)
                    self.caducidadexpires()
                print(' ')
                mensajelinea = messageSplit.replace('\r\n', ' ')
                mensajelinea = ('Received from... ' +
                                '127.0.0.1:' + str(clientPort)
                                + ' ' + mensajelinea)
                write_log(mensajelinea, log)

            elif method == 'INVITE' or 'ACK' or 'BYE':
                mensajerecortado = messageSplit.split(' ')
                usuario = mensajerecortado[1].split(':')[1]
                if usuario in self.diccionario:
                    with socket.socket(socket.AF_INET,
                                       socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET,
                                             socket.SO_REUSEADDR, 1)

                        try:
                            ip = self.diccionario[usuario]['address']
                            puerto = int(self.diccionario[usuario]['port'])
                            my_socket.connect((ip, puerto))
                            print("Enviando ", messageSplit)
                            my_socket.send(bytes(messageSplit, 'utf-8')
                                           + b'\r\n')
                            trans = messageSplit.replace('\r\n', ' ')
                            transcripcion0 = ('Send to... ' + str(ip) +
                                              ':' + str(puerto)
                                              + ' ' + trans)
                            write_log(transcripcion0, log)
                            data = my_socket.recv(1024)
                            recibido = data.decode('utf-8')
                            recibido = recibido.replace('\r\n', ' ')
                            recibido = ('Received from... ' + str(ip) +
                                        ':' + str(puerto)
                                        + ' ' + recibido)
                            write_log(recibido, log)
                            self.wfile.write(data)

                        except ConnectionRefusedError:
                            sys.exit('')
                        print("Socket terminado")
                else:
                    self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
            else:
                self.wfile.write(b'Method Not Allowed\r\n\r\n')


if __name__ == "__main__":

    try:
        config = sys.argv[1]

    except IndexError:

        sys.exit("Usage: python3 proxy_registrar.py config ")

    parser = make_parser()
    cHandler = UApHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('pr.xml'))
    xml = cHandler.get_tags()

    nombreproxy = xml[0][1]["name"]
    serverproxy = xml[0][1]["ip"]
    portproxy = xml[0][1]["puerto"]
    datapath = xml[1][1]["path"]
    datapasswd = xml[1][1]["passwdpath"]
    log = xml[2][1]["log"]
    serv = socketserver.UDPServer((serverproxy, int(portproxy)), EchoHandler)
    print("Lanzando proxy UDP de eco...")
    serv.serve_forever()
