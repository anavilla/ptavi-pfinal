#!/usr/bin/python3
# -*- coding: utf-8 -*-
import socket
import sys
import time
import secrets
import random
import simplertp
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class UAcHandler(ContentHandler):

    def __init__(self):
        self.list = []
        self.log_uno = False
        self.audio_uno = False
        self.diccionario = {"account": ["username", "passwd"],
                            "uaserver": ["ip", "puerto"],
                            "rtpaudio": ["puerto"],
                            "regproxy": ["ip", "puerto"]
                            }

    def startElement(self, name, attrs):
        if name in self.diccionario:
            dicci = {}
            for i in self.diccionario[name]:
                dicci[i] = attrs.get(i, "")
            self.list.append([name, dicci])
        elif name == "log":
            self.log_uno = True
        elif name == "audio":
            self.audio_uno = True

    def characters(self, char):
        # al encontrar etiqueta sin atributo lee entre medias
        if self.log_uno is True:
            self.list.append(["log", {"log": char}])
            self.log_uno = False
        if self.audio_uno is True:
            self.list.append(["audio", {"audio": char}])
            self.audio_uno = False

    def get_tags(self):
        return self.list


def write_log(evento, log):
    fichero = open(log, 'a')
    tiempo = time.strftime('%Y%m%d%H%M%S')
    fichero.write(tiempo)
    fichero.write(' ')
    fichero.write(evento)
    fichero.write("\r\n")
    fichero.close()


if __name__ == "__main__":
    try:
        config = str(sys.argv[1])
        method = str(sys.argv[2])
        option = str(sys.argv[3])
        # Enviando = "Starting..."
        # print(Enviando)
    except IndexError:
        sys.exit(" Usage: python3 client.py config method option")

    if len(sys.argv) != 4:
        sys.exit('Usage: python uaclient.py config method option')

    parser = make_parser()
    cHandler = UAcHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(config))
    xml = cHandler.get_tags()

    username = xml[0][1]["username"]
    passwd = xml[0][1]["passwd"]
    SERVER = xml[1][1]["ip"]
    PORT = xml[1][1]["puerto"]
    rtport = xml[2][1]["puerto"]
    regproxyip = xml[3][1]["ip"]
    regproxyport = xml[3][1]["puerto"]
    audio = xml[5][1]["audio"]
    log = xml[4][1]["log"]

    if method == "REGISTER":
        line = ("REGISTER " + "sip:" + username + ':' + PORT + ' SIP/2.0\r\n'
                + "Expires: " + option + ' \r\n')
        trans = line.replace('\r\n', ' ')
        transcripcion1 = ("Sent to... " + regproxyip +
                          ':' + regproxyport + ' ' + trans)
        write_log(transcripcion1, log)

    elif method == "INVITE":
        SDP = ('v=0\r\n'
               + 'o=' + username + ' ' + SERVER + '\r\n'
               + 's=misesion\r\n'
               + 't=0\r\n'
               + 'm=audio ' + rtport + ' RTP')
        length_sdp = str(len(bytes(SDP, 'utf-8')))
        line = (method + ' sip:' + option + ' SIP/2.0\r\n'
                + 'Content-Type: application/sdp\r\n'
                + 'Content-Length: ' + length_sdp + '\r\n\r\n'
                + SDP + '\r\n')
        trans = line.replace('\r\n', ' ')
        transcripcion1 = ("Sent to... " + regproxyip +
                          ':' + regproxyport + ' ' + trans)
        write_log(transcripcion1, log)

    elif method == "BYE":
        line = (method + " sip:" + option + " SIP/2.0\r\n")
        trans = line.replace('\r\n', ' ')
        transcripcion1 = ("Sent to... " + regproxyip +
                          ':' + regproxyport + ' ' + trans)
        write_log(transcripcion1, log)
    else:
        print(" Usage: python3 client.py config method option")

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        try:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((regproxyip, int(regproxyport)))
            print("Enviando ", line)
            my_socket.send(bytes(line, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024)
            respuestaServidor = data.decode('utf-8')
            print("Recibido", respuestaServidor)
            if method == 'INVITE':
                try:
                    respuestaServidorc = respuestaServidor.split(' ')
                    puertortp = respuestaServidorc[-2]
                    cortado = respuestaServidorc[-3]
                    cortado = cortado.split('\r\n')
                    iprtp = cortado[0]
                    if respuestaServidorc[1] == '100':
                        ACK_FRASE = ('ACK ' + 'sip:' + option
                                     + ' SIP/2.0\r\n')
                    print("Send to... " + regproxyip
                          + ':' + regproxyip
                          + ' ' + ACK_FRASE)
                    transcripcion2 = ('Send to... ' + regproxyip
                                      + ':' + regproxyip
                                      + ' ' + ACK_FRASE)
                    write_log(transcripcion2, log)
                    my_socket.send(bytes(ACK_FRASE, 'utf-8') + b'\r\n')
                    ALEAT = random.randint(0, 1000)
                    BIT = secrets.randbits(1)
                    n = random.randrange(0, 15)
                    csrc = [random.randint(0, 2 ** 10) for _ in range(n)]
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(version=2, pad_flag=0,
                                          ext_flag=0,
                                          cc=len(csrc), marker=BIT,
                                          payload_type=14,
                                          ssrc=ALEAT)
                    RTP_header.setCSRC(csrc)
                    audiofi = simplertp.RtpPayloadMp3(audio)
                    simplertp.send_rtp_packet(RTP_header, audiofi,
                                              iprtp, int(puertortp))
                except Exception:
                    sys.exit('')
        except ConnectionRefusedError:
            transcripcion3 = ("20101018160243 Error: No server listening at "
                              + regproxyip + "port " + regproxyport)
            write_log(transcripcion3, log)
            sys.exit("20101018160243 Error: No server listening at "
                     + regproxyip + "port " + regproxyport)
