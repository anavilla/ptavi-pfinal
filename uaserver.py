#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import socketserver
import random
import simplertp
import secrets
from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class UAsHandler(ContentHandler):

    def __init__(self):
        self.log_uno = False
        self.audio_uno = False
        self.list = []
        self.diccionario = {"account": ["username", "passwd"],
                            "uaserver": ["ip", "puerto"],
                            "rtpaudio": ["puerto"],
                            "regproxy": ["ip", "puerto"]}

    def startElement(self, name, attrs):

        if name in self.diccionario:
            list = {}
            for i in self.diccionario[name]:
                list[i] = attrs.get(i, "")
            self.list.append([name, list])
        elif name == "log":
            self.log_uno = True
        elif name == "audio":
            self.audio_uno = True

    def characters(self, char):
        # al encontrar etiqueta sin atributo lee entre medias

        if self.log_uno is True:
            self.list.append(["log", {"log": char}])
            self.log_uno = False
        if self.audio_uno is True:
            self.list.append(["audio", {"audio": char}])
            self.audio_uno = False

    def get_tags(self):
        return self.list


puertortp = ''
iprtp = ''


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    diccionarioRTP = {}

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            print("El cliente nos manda " + line.decode('utf-8'))
            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
            mensaje = line.decode('utf-8')
            method = mensaje.split(' ')[0]
            print("El cliente nos manda: " + method)

            if method == 'INVITE':
                global puertortp
                global iprtp
                mensaje = mensaje.split(' ')
                puertortp = mensaje[-2]
                cortado1 = mensaje[-3]
                cortado2 = cortado1.split('\r\n')
                iprtp = cortado2[0]
                self.diccionarioRTP['Origen'] = {'puertortp': puertortp,
                                                 'iprtp': iprtp}

                m = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 "
                m += "Ringing\r\n\r\nSIP/2.0 200 OK\r\n\r\n"
                # SDP
                SDP = ('v=0\r\n'
                       + 'o=' + username + ' ' + SERVER + '\r\n'
                       + 's=misesion\r\n'
                       + 't=0\r\n'
                       + 'm=audio ' + rtport + ' RTP')
                length_sdp = str(len(bytes(SDP, 'utf-8')))
                m = (m + 'Content-Type: application/sdp\r\n'
                     + 'Content-Length: ' + length_sdp + '\r\n\r\n'
                     + SDP + '\r\n')
                self.wfile.write(bytes(m, 'utf-8'))

            elif method == 'ACK':
                ALEAT = random.randint(0, 1000)
                BIT = secrets.randbits(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, pad_flag=0, ext_flag=0,
                                      cc=0, marker=BIT, payload_type=14,
                                      ssrc=ALEAT)
                audiofi = simplertp.RtpPayloadMp3(audio)
                simplertp.send_rtp_packet(RTP_header, audiofi,
                                          iprtp, int(puertortp))
            elif method == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif method != 'INVITE' or method != 'ACK' or method != 'BYE':
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":

    try:
        config = sys.argv[1]

        print("Listenning...")

    except IndexError:
        sys.exit("Usage: python3 uaserver.py config ")

    if len(sys.argv) != 2:
        sys.exit('Usage: python uaserver.py config')

    parser = make_parser()
    cHandler = UAsHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(config))
    xml = cHandler.get_tags()

    username = xml[0][1]["username"]
    passwd = xml[0][1]["passwd"]
    SERVER = xml[1][1]["ip"]
    PORT = xml[1][1]["puerto"]
    print(PORT)
    rtport = xml[2][1]["puerto"]
    regproxyip = xml[3][1]["ip"]
    regproxyport = xml[3][1]["puerto"]
    log2 = xml[4][1]["log"]
    audio = xml[5][1]["audio"]
    serv = socketserver.UDPServer((SERVER, int(PORT)), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
